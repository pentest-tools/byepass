## Project Announcements

* Twitter: [https://twitter.com/webpwnized](https://twitter.com/webpwnized)

## Installation

YouTube deleted the video tutorials. YouTube deletes videos related to security testing at any time without
reviewing the content to understand the contents educational intent. 

**More details?** [Installation Document](README-INSTALL.md)

## Usage

YouTube deleted the video tutorials. YouTube deletes videos related to security testing at any time without
reviewing the content to understand the contents educational intent.

**More details?** 

* [ByePass: Usage Instructions](README-USAGE-BYEPASS.md)
* [PassTime: Usage Instructions](README-USAGE-PASSTIME.md)

## More Help

YouTube deleted the video tutorials. YouTube deletes videos related to security testing at any time without
reviewing the content to understand the contents educational intent. 
